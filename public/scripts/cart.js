document.addEventListener('DOMContentLoaded',function(){
    let moins=document.getElementsByClassName('moins')
    let plus=document.getElementsByClassName('plus')
    let total=document.getElementById('total')
    for(let p of plus){
        p.addEventListener('click',function(){
            let quantite=p.previousElementSibling
            let nbreplus=parseInt(quantite.textContent)
            if(nbreplus<5){
                nbreplus+=1
                quantite.textContent=String(nbreplus)
                total.textContent=String(Price())
                if(nbreplus==5) {
                    p.nextElementSibling.style.visibility='visible'
                }
            }
        })
    }

    for(let m of moins){
        m.addEventListener('click',function(){
            let quantite=m.nextElementSibling
            let nbremoins=parseInt(quantite.textContent)
            if(nbremoins>1){
                nbremoins-=1
                quantite.textContent=String(nbremoins)
                total.textContent=String(Price())
                if(nbremoins===4){
                    m.nextElementSibling.nextElementSibling.nextElementSibling.style.visibility='hidden'
                }
            }
        })
    }

    function Price(){
        let price=0;
        let product;
        let nbre;
        for(let elem of moins){
             product=parseInt(elem.previousElementSibling.textContent)
             nbre=parseInt(elem.nextElementSibling.textContent)
            price+=product*nbre
        }
        return price
    }
    total.textContent=String(Price())
})
