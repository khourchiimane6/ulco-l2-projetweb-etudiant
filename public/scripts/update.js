document.addEventListener('DOMContentLoaded',function() {
    var firstname = document.getElementById('firstname_up')
    var lastname = document.getElementById('lastname_up')
    var mail = document.getElementById('mail_up')
    var mailRegExp = new RegExp(/^([a-z0-9._-]+)@([a-z0-9._-]+)\.([a-z]{2,6})$/);


    firstname.addEventListener('keyup',function(){
        if(firstname.value.length<2) {
            firstname.classList.remove("valid");
            firstname.classList.add("invalid");
        }
        else {
            firstname.classList.remove("invalid");
            firstname.classList.add("valid");
        }
    })

    lastname.addEventListener('keyup',function(){
        if(lastname.value.length<2) {
            lastname.classList.remove("valid");
            lastname.classList.add("invalid");
        }
        else {
            lastname.classList.remove("invalid");
            lastname.classList.add("valid");
        }
    })

    mail.addEventListener('keyup',function(){
        if(!mailRegExp.test(mail.value)) {
            mail.classList.remove("valid");
            mail.classList.add("invalid");
        }
        else {
            mail.classList.remove("invalid");
            mail.classList.add("valid");
        }
    })
    formup.addEventListener('submit',function(event){

        event.preventDefault();
        let invalid=document.getElementsByClassName('invalid')
        if(invalid.length===0) {
            formup.submit();
        }
    })
})
