document.addEventListener('DOMContentLoaded',function(){
    var firstname=document.getElementById('firstname')
    var lastname=document.getElementById('lastname')
    var mail=document.getElementById('email')
    var password=document.getElementById('pwd')
    var password1=document.getElementById('pwd1')
    var form=document.getElementById('form')
    var mailRegExp = new RegExp(/^([a-z0-9._-]+)@([a-z0-9._-]+)\.([a-z]{2,6})$/);
    var mdpReg= new RegExp(/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]){6,}$/);


    firstname.addEventListener('keyup',function(){
        if(firstname.value.length<2) {
            firstname.classList.remove("valid");
            firstname.classList.add("invalid");
            firstname.previousElementSibling.classList.remove('valid');
            firstname.previousElementSibling.classList.add('invalid');

        }
        else {
            firstname.classList.remove("invalid");
            firstname.classList.add("valid");
            firstname.previousElementSibling.classList.remove('invalid');
            firstname.previousElementSibling.classList.add('valid');
        }
    })

    lastname.addEventListener('keyup',function(){
        if(lastname.value.length<2) {
            lastname.classList.remove("valid");
            lastname.classList.add("invalid");
            lastname.previousElementSibling.classList.remove('valid');
            lastname.previousElementSibling.classList.add('invalid');

        }
        else {
            lastname.classList.remove("invalid");
            lastname.classList.add("valid");
            lastname.previousElementSibling.classList.remove('invalid');
            lastname.previousElementSibling.classList.add('valid');
        }
    })

    mail.addEventListener('keyup',function(){
        if(!mailRegExp.test(mail.value)) {
            mail.classList.remove("valid");
            mail.classList.add("invalid");
            mail.previousElementSibling.classList.remove('valid');
            mail.previousElementSibling.classList.add('invalid');
        }
        else {
            mail.classList.remove("invalid");
            mail.classList.add("valid");
            mail.previousElementSibling.classList.remove('invalid');
            mail.previousElementSibling.classList.add('valid');
        }
    })

    password.addEventListener('keyup',function(){
        if(!mdpReg.test(password.value)){
            password.classList.remove("valid");
            password.classList.add("invalid");
            password.previousElementSibling.classList.remove('valid');
            password.previousElementSibling.classList.add('invalid');
        }
        else {
            password.classList.remove("invalid");
            password.classList.add("valid");
            password.previousElementSibling.classList.remove('invalid');
            password.previousElementSibling.classList.add('valid');
        }
    })

    password1.addEventListener('keyup',function(){
        if(password.value===password1.value) {
            password1.classList.remove("invalid");
            password1.classList.add("valid");
            password1.previousElementSibling.classList.remove('valid');
            password1.previousElementSibling.classList.add('invalid');
        }
        else {
            password1.classList.remove("valid");
            password1.classList.add("invalid");
            password1.previousElementSibling.classList.remove('invalid');
            password1.previousElementSibling.classList.add('valid');
        }

    })

    form.addEventListener('submit',function(event){

        event.preventDefault();
        let invalid=document.getElementsByClassName('invalid')
        if(invalid.length===0) {
            form.submit();
        }

    })
})