<?php


namespace model;


class CommentModel
{
    static function listComment(int $id_product){
        $db=\model\Model::Connect();
        $sql="SELECT comment.content,comment.date,account.firstname,account.lastname FROM comment INNER JOIN account ON comment.id_account=account.id WHERE comment.id_product=$id_product";
        $req=$db->prepare($sql);
        $req->execute();
        return $req->fetchAll();
    }
    static function insertComment(int $id_product, int $id_account, string $content ){
        $db=\model\Model::Connect();
        $sql="INSERT INTO comment ( content,date,id_product,id_account) VALUES('$content',now(),'$id_product','$id_account')";
        $req=$db->prepare($sql);
        $req->execute();
    }
}