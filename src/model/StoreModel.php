<?php

namespace model;

class StoreModel {

  static function listCategories(): array
  {
    // Connexion à la base de données
    $db = \model\Model::connect();

    // Requête SQL
    $sql = "SELECT id, name FROM category";
    
    // Exécution de la requête
    $req = $db->prepare($sql);
    $req->execute();

    // Retourner les résultats (type array)
    return $req->fetchAll();
  }
  static function listProducts():array{
      $db1 = \model\Model::connect();
      // Requête SQL
        $sql1 = "SELECT p.id as id_pr , p.name as name_pr, p.price as price_pr, p.image as img_pr, c.name as name_cat FROM product as p INNER JOIN category as c ON (p.category=c.id)";

      // Exécution de la requête
      $req1 = $db1->prepare($sql1);
      $req1->execute();

      // Retourner les résultats (type array)
      return $req1->fetchAll();
  }
  static function infoProduct(int $id){
      $db1 = \model\Model::connect();
      // Requête SQL
      $sql1 = "SELECT  p.id as id_pr ,p.name as name_pr, p.price as price_pr, p.image as img_pr, p.spec as spec_pr, c.name as name_cat FROM product as p INNER JOIN category as c ON (p.category=c.id) WHERE  p.id = $id";

          // Exécution de la requête
          $req1 = $db1->prepare($sql1);
          $req1->execute();

          // Retourner les résultats (type array)
          return $req1->fetch();

  }

  static function SearchProduct(string  $search,$category,$ordre):array
  {
      $db1 = \model\Model::connect();
      if ($search != null and $category == null and $ordre == null) {
          $sql1 = ' SELECT p.id as id_pr , p.name as name_pr, p.price as price_pr, p.image as img_pr, c.name as name_cat FROM product as p INNER JOIN category as c ON (p.category=c.id) WHERE p.name LIKE :pattern';
          // Exécution de la requête
          $req1 = $db1->prepare($sql1);
          $req1->execute(array(
              "pattern" => "%" . $search . "%"
          ));

          // Retourner les résultats (type array)
          return $req1->fetchAll();
      }
      if ($search == null and $category != null and $ordre == null) {
          $sql1 = "SELECT p.id as id_pr , p.name as name_pr, p.price as price_pr, p.image as img_pr, c.name as name_cat FROM product as p INNER JOIN category as c ON (p.category=c.id)  WHERE c.name LIKE :pattern";
          $req1 = $db1->prepare($sql1);
          foreach ($category as $c) {
              $cat = $c;
          }
      $req1->execute(array(
          "pattern" => "%" . $cat . "%"
      ));
          return $req1->fetchAll();

      }
      if($search==null and $category==null and $ordre!=null ) {
          if($ordre=="croissant"){
              $sql1="SELECT p.id as id_pr , p.name as name_pr, p.price as price_pr, p.image as img_pr, c.name as name_cat FROM product as p INNER JOIN category as c ON (p.category=c.id) ORDER BY p.price ASC";
          }
          else if($ordre=="decroissant"){
              $sql1="SELECT p.id as id_pr , p.name as name_pr, p.price as price_pr, p.image as img_pr, c.name as name_cat FROM product as p INNER JOIN category as c ON (p.category=c.id) ORDER BY p.price DESC";
          }
          $req1 = $db1->prepare($sql1);
          $req1->execute();
          return $req1->fetchAll();
      }
      if ($search != null and $category != null and $ordre == null) {
          $sql1 = ' SELECT p.id as id_pr , p.name as name_pr, p.price as price_pr, p.image as img_pr, c.name as name_cat FROM product as p INNER JOIN category as c ON (p.category=c.id) WHERE (p.name LIKE :pattern and c.name LIKE :pattern1)';
          foreach ($category as $c) {
              $cat = $c;
          }
          $req1 = $db1->prepare($sql1);
          $req1->execute(array(
              "pattern" => "%" . $search . "%",
              "pattern1"=> "%" . $cat . "%"
          ));
          return $req1->fetchAll();
      }
      if ($search != null and $category == null and $ordre != null) {
          if($ordre=="croissant"){
              $sql1="SELECT p.id as id_pr , p.name as name_pr, p.price as price_pr, p.image as img_pr, c.name as name_cat FROM product as p INNER JOIN category as c ON (p.category=c.id)  WHERE p.name LIKE :pattern ORDER BY p.price ASC";
          }
          else if($ordre=="decroissant"){
              $sql1="SELECT p.id as id_pr , p.name as name_pr, p.price as price_pr, p.image as img_pr, c.name as name_cat FROM product as p INNER JOIN category as c ON (p.category=c.id) WHERE p.name LIKE :pattern ORDER BY p.price DESC";
          }
          $req1 = $db1->prepare($sql1);
          $req1->execute(array(
              "pattern" => "%" . $search . "%"
          ));
          return $req1->fetchAll();
      }
      if ($search == null and $category != null and $ordre != null) {
          if($ordre=="croissant"){
              $sql1="SELECT p.id as id_pr , p.name as name_pr, p.price as price_pr, p.image as img_pr, c.name as name_cat FROM product as p INNER JOIN category as c ON (p.category=c.id)  WHERE c.name LIKE :pattern ORDER BY p.price ASC";
          }
          else if($ordre=="decroissant"){
              $sql1="SELECT p.id as id_pr , p.name as name_pr, p.price as price_pr, p.image as img_pr, c.name as name_cat FROM product as p INNER JOIN category as c ON (p.category=c.id) WHERE c.name LIKE :pattern ORDER BY p.price DESC";
          }
          $req1 = $db1->prepare($sql1);
          foreach ($category as $c) {
              $cat = $c;
          }
          $req1->execute(array(
              "pattern" => "%" . $cat . "%"
          ));
          return $req1->fetchAll();
      }
      if ($search != null and $category != null and $ordre != null) {
          if($ordre=="croissant"){
              $sql1="SELECT p.id as id_pr , p.name as name_pr, p.price as price_pr, p.image as img_pr, c.name as name_cat FROM product as p INNER JOIN category as c ON (p.category=c.id)  WHERE (c.name LIKE :pattern1 and p.name LIKE :pattern) ORDER BY p.price ASC";
          }
          else if($ordre=="decroissant"){
              $sql1="SELECT p.id as id_pr , p.name as name_pr, p.price as price_pr, p.image as img_pr, c.name as name_cat FROM product as p INNER JOIN category as c ON (p.category=c.id) WHERE (c.name LIKE :pattern1 and p.name LIKE :pattern) ORDER BY p.price DESC";
          }
          $req1 = $db1->prepare($sql1);
          foreach ($category as $c) {
              $cat = $c;
          }
          $req1->execute(array(
              "pattern" => "%" . $search . "%",
              "pattern1"=> "%" . $cat . "%"
          ));
          return $req1->fetchAll();
      }


      }
}