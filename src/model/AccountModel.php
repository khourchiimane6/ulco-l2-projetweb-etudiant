<?php


namespace model;


class AccountModel
{
    static function check($firstname, $lastname, $mail, $password) : bool{
        if(strlen($firstname)>=2 and  strlen($lastname)>=2  and filter_var($mail, FILTER_VALIDATE_EMAIL) and (strlen($password) >= 6)) {
            $db = \model\Model::connect();
            $sql = "SELECT * FROM account WHERE `mail`='$mail'";
            $req = $db->prepare($sql);
            $req->execute();
            $req1=$req->fetch();
            if (!$req1) {
                return true;
            }
        }
        return false;

    }
    static function signin($firstname, $lastname, $mail, $password) : bool{
        if(self::check($firstname, $lastname, $mail, $password)){
            $db = \model\Model::connect();
            $var=password_hash($password,PASSWORD_DEFAULT );
            $sql = "INSERT INTO `account`(`firstname`, `lastname`, `mail`, `password`) VALUES (:firstname, :lastname, :mail, :password)";
            $req = $db->prepare($sql);
            //$req->execute();
            $req->execute(array(
                "firstname" => $firstname,
                "lastname" => $lastname,
                "mail" => $mail,
                "password" => $var,
            ));
            return true;
        }
        return false;
    }
    static function login($mail, $password) {
        $db = \model\Model::connect();
        $sql = "SELECT * FROM `account` WHERE account.mail='$mail'";
        $req = $db->prepare($sql);
        $req->execute();
        return $req->fetch();
    }
   static function upInfos($lastname, $firstname,$mail,$id):bool{
       if(strlen($firstname)>=2 and  strlen($lastname)>=2  and filter_var($mail, FILTER_VALIDATE_EMAIL)) {
           $db = \model\Model::connect();
           $sql = "UPDATE account SET firstname='$firstname', lastname='$lastname', mail='$mail' WHERE account.id=$id";
           $req = $db->prepare($sql);
           $req->execute();
           return true;
       }
       return false;
   }
}