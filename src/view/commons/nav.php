<nav>
  <img src="/public/images/logo.jpeg" alt="logo" />
    <a href="/">
        ACCEUIL
    </a>
    <a href="/store">
        BOUTIQUE
    </a>
    <?php
        if(isset($_SESSION['statut']) ):
           ?>
        <a class="account" href="/account/infos">
            <img src="/public/images/avatar.png" alt="image de avatar" />
            <?= $_SESSION['prénom']." ".$_SESSION['nom']?></a>
        <a href="/cart" >PANIER</a>
        <a href="/account/logout" >DECONECTION</a>
        <?php
        else : ?>
            <a class="account" href="/account">
                  <img src="/public/images/avatar.png" alt="image de avatar" />
                  COMPTE
            </a>
    <?php
    endif;
        ?>

</nav>