<div id="product">
   <?php $cmp=$params['info'];
   ?>
    <div>
        <div class="product-images">
                <img src="/public/images/<?= $cmp['img_pr']?>" id="img"/>
                <div class="product-miniatures">
                        <div>
                            <img src="/public/images/<?= $cmp['img_pr']?>" class="pic"/>
                        </div>
                    <?php for($i=1;$i<=3;$i++):?>
                        <div>
                            <img src="/public/images/<?=str_replace(".jpg","_alt$i.jpg",$cmp['img_pr'])?>" class="pic"/>
                        </div>
                    <?php endfor;?>
            </div>
        </div>

        <div class="product-infos">
            <p class="product-category">
                <?php echo $cmp['name_cat'] ?>
            </p>
            <h1>
                <?php echo $cmp['name_pr'] ?>
            </h1>
            <p class="product-price">
                <?php echo $cmp['price_pr'] ?>
            </p>
            <form method="post" action="/cart/add">
                <button type="button" class="btn_moins">-</button>
                <button type="button" id="nbre">1</button>
                <button type="button" class="btn_plus">+</button>
                <input type="submit" value="Ajouter au panier"/>
                <input type="hidden" value="<?= $cmp['id_pr']?>" name="id_product" >
                <input type="hidden" value="<?= $cmp['img_pr']?>" name="img_product" >
                <input type="hidden" value="<?= $cmp['name_cat']?>" name="name_category" >
                <input type="hidden" value="<?= $cmp['name_pr']?>" name="name_product" >
                <input type="hidden" value="<?= $cmp['price_pr']?>" name="price_product" >
                <input type="hidden" name="quantite" id="quantité" value="1" >

            </form>
            <div class="box error" style="visibility:hidden" id="box">
                Quantité maximale autorisée !
            </div>
        </div>
    </div>
    <div>
        <div class="product-spec">
            <h2>Spécifités</h2>
            <?php echo $cmp['spec_pr'] ?>
        </div>
        <div class="product-comments">
            <h2>Avis</h2>
            <?php if(!empty($params['comment'])):?>
                <?php foreach($params['comment'] as $comment):?>
                    <div class="product-comment">
                        <p class="product-comment-author">
                            <?php echo $comment['firstname']." ".$comment['lastname']?>
                        </p>
                        <p>
                            <?php echo $comment['content']?>
                            Date : <?php echo $comment['date']?>
                        </p>

                    </div>
                <?php endforeach;?>
            <?php else:?>
                <p>Il n'y a pas d'avis pour ce produit.</p>
            <?php endif;?>
            <?php if(isset($_SESSION['id'])):?>
                <form  method="post" action="/postComment/<?php echo $cmp['id_pr']?>">
                    <p><input type="text" name="content" placeholder="Rediger un commentaire"/></p>
                    <p><input type="submit" value="Publier" /></p>
                </form>
            <?php endif;?>

        </div>
    </div>
</div>
<script src="/public/scripts/product.js"></script>



