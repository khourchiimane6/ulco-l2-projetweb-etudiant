<?php if(!empty($_GET['StatusUp'])):?>
        <?php if($_GET['StatusUp']=="echec"):?>
            <div class="box error" >
                 Echec de mis à jour verifiez vos informations ou le mail que vous avez donneé existe deja
            </div>
        <?php endif;?>
        <?php if($_GET['StatusUp']=="succès"):?>
            <div class="box info">
                  Tes informations personnelles ont été mis à jour.
             </div>
        <?php endif;?>
<?php endif;?>
<div id="account">
    <form class="account-signin" method="post" action="/account/update" >

        <h2>Informations du Compte</h2>
        <h3>Informations personnelles</h3>
        <p>Nom</p>
        <input type="text" name="lastname" placeholder="Nom" value="<?=$_SESSION['nom']?>" id="lastname_up"/>

        <p>Prénom</p>
        <input type="text" name="firstname" placeholder="Prénom" value="<?=$_SESSION['prénom']?>" id="firstname_up"/>

        <p>Adresse mail</p>
        <input type="text" name="mail" placeholder="Adresse mail" value="<?=$_SESSION['email']?>" id="mail_up" />

        <input type="submit" id="formuup" value="Modifier vos informations" />

    </form>
</div>
<script src="/public/scripts/update.js"></script>