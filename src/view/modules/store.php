<div id="store">

<!-- Filtrer l'affichage des produits  ---------------------------------------->

<form method="post" action="/store/search">

  <h4>Rechercher</h4>
  <input type="text" name="search" placeholder="Rechercher un produit" />

  <h4>Catégorie</h4>
  <?php foreach ($params["categories"] as $c) { ?>
    <input type="checkbox" name="category[]" value="<?= $c["name"] ?>" />
    <?= $c["name"] ?>
    <br/>
  <?php } ?>

  <h4>Prix</h4>
  <input type="radio" name="order" value="croissant" /> Croissant <br />
  <input type="radio" name="order" value="decroissant" /> Décroissant <br />

  <div><input type="submit" value="Appliquer" /></div>

</form>

<!-- Affichage des produits --------------------------------------------------->

<div class="products">

<!-- TODO: Afficher la liste des produits ici -->
    <?php
    foreach($params['product'] as $tmp)
    {
        ?>
    <div class="card">
        <p class="card-image">
            <img src="/public/images/<?php echo $tmp['img_pr'] ?>">
        </p>
        <p class="card-category">
            <?php echo $tmp['name_cat'] ?>
        </p>
        <P class="card-title">
            <a href="/store/<?php echo$tmp['id_pr']?>">
                <?php echo $tmp['name_pr'] ?>
            </a>
        </P>
        <P class="card-price">
            <?php echo $tmp['price_pr'] ?> €
        </P>
    </div>
    <?php } ?>

</div>

</div>
