<h1 style="text-align: center" >Panier</h1>
<?php if(($params['cart'])==null):?>
    <h3>Votre panier est Vide ajouter un produit dans le panier se trouvant dans la boutique </h3>
<?php else:?>
    <div id="store">
        <div class="products">
        <?php foreach ($params['cart'] as $c):?>
                    <div >
                         <p class="card-image"><img src="/public/images/<?=$c['img']?>"/></p>
                         <p class="card-category"><?=$c['name_category']?></p>
                         <p class="card-title"><?=$c['name_product']?></p>
                         <p class="card-price"><?=$c['price']?>€</p>
                         <button type="button" class="moins">-</button>
                         <button type="button"><?=$c['quantite']?></button>
                         <button type="button" class="plus">+</button>
                         <div class="box error"  style="visibility:hidden">
                             Quantité maximale autorisée !
                         </div>
                    </div>
              <?php endforeach;?>
        </div>
    </div>
    <h3 style="border:solid">Prix Total du Panier: <span id="total"></span>€ </h3>

<?php endif;?>
<script src="/public/scripts/cart.js"></script>
