<?php


namespace controller;


class CartController
{
    public function cart(){
        $id=$_SESSION['id'];
        $cart=\model\CartModel::SelectInCart($id);
        $params=array(
            "title"=>"Cart",
            "module"=>"cart.php",
            "cart"=>$cart
        );
        \view\Template::render($params);
    }
    public function add()
    {
        if (!isset($_SESSION['id'])) {
            header("Location: /account");
            exit();
        }
            $id_produit = $_POST['id_product'];
            $img = $_POST['img_product'];
            $prod =$_POST['name_product'];
            $cat = $_POST['name_category'];
            $price =$_POST['price_product'];
            $quantite =$_POST['quantite'];
            $id=$_SESSION['id'];
            \model\CartModel::insertInCart($img,$prod,$price,$quantite,$cat,$id_produit,$id);
            header("Location: /cart");
            exit();

    }


}