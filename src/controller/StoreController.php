<?php

namespace controller;

class StoreController {

  public function store(): void
  {
    // Communications avec la base de données
    $categories = \model\StoreModel::listCategories();
    $product = \model\StoreModel::listProducts();


      // Variables à transmettre à la vue
    $params = array(
      "title" => "Store",
      "module" => "store.php",
      "categories" => $categories,
      "product" => $product
    );

    // Faire le rendu de la vue "src/view/Template.php"
    \view\Template::render($params);

  }
  public function  product(int $id){
      $info = \model\StoreModel::infoProduct($id);
      $comment = \model\CommentModel::listComment($id);

      if($info==null){
          header("Location: /store");
          exit();
      }
      else{
          $params = array(
              "title" => "Product",
              "module" => "product.php",
              "info" => $info,
              "comment" => $comment
          );

          \view\Template::render($params);
      }
  }
    public function search()
    {
        $category=[];
        $order=[];
        $search=htmlspecialchars($_POST['search']);
        if (isset($_POST['category']) && !empty($_POST['category'])){
            $category= $_POST['category'];
        }
        if (isset($_POST['order']) && !empty($_POST['order'])) {
            $order = $_POST['order'];
        }

        $categories = \model\StoreModel::listCategories();
        $product = \model\StoreModel::SearchProduct($search,$category,$order);
        $params = array(
            "title" => "Store",
            "module" => "store.php",
            "categories" => $categories,
            "product" => $product
        );

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);

    }

}