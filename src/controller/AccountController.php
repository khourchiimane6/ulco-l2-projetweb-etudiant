<?php


namespace controller;


class AccountController
{
    public function account(): void{
        if(empty($_SESSION['id'])){
            $params= array(
                "title"=> "account",
                "module"=>"account.php"//c'est de ça qu'il s'agit
            );
            // Faire le rendu de la vue "src/view/Template.php"
            \view\Template::render($params);
        }
        else{
            header("Location: /account/infos");
            exit();

        }
    }
    public function signin():void
    {
        $userlastname = htmlspecialchars($_POST['userlastname']);
        $userfirstname = htmlspecialchars($_POST['userfirstname']);
        $usermail = htmlspecialchars($_POST['usermail']);
        $userpass = htmlspecialchars($_POST['userpass']);

        $singin = \model\AccountModel::signin($userfirstname, $userlastname, $usermail, $userpass);
        if ($singin) {
            header("Location: /account?status=signin_succes");
            exit();
        }
        else{
            header("Location: /account?status=signin_fail");
            exit();
        }
    }
        public function login():void{
            $statut=false;
            $usermail = htmlspecialchars($_POST['usermail']);
            $userpass = htmlspecialchars($_POST['userpass']);

            $login = \model\AccountModel::login($usermail, $userpass);
           if(password_verify($userpass,$login['password']) and $login!=false){
                 $statut=true;
                 $_SESSION['statut']=$statut;
                 $_SESSION['id']=$login['id'];
                 $_SESSION['prénom']=$login['firstname'];
                 $_SESSION['nom']=$login['lastname'];
                 $_SESSION['email']=$login['mail'];
                 $_SESSION['mdp']=$login['password'];
                 header("Location: /store");
                 exit();
             }
            header("Location: /account?status=login_fail");
           //  exit();
        }
        public function logout():void{
            session_destroy();
            header("Location: /account?status=logout_succes");
            //exit();
        }
        public function infos():void{
          if(isset($_SESSION['email'],$_SESSION['prénom'],$_SESSION['nom'])){
                $params=array(
                    "title"=>"account information",
                    "module"=>"infos.php"
                );
                \view\Template::render($params);
           }else{
                header('Location: /account');
                exit();
            }
        }
    public function update()
    {
        if (isset($_SESSION['email'], $_SESSION['prénom'], $_SESSION['nom'])) {
            $lastname = htmlspecialchars($_POST['lastname']);
            $firstname = htmlspecialchars($_POST['firstname']);
            $mail = htmlspecialchars($_POST['mail']);
            $id = $_SESSION['id'];

            if(\model\AccountModel::upInfos($lastname, $firstname, $mail, $id)) {
                $_SESSION['nom'] = $lastname;
                $_SESSION['prénom'] = $firstname;
                $_SESSION['email'] = $mail;
                header('Location: /account/infos?StatusUp=succès');
                exit();
            }
            else{
                header('Location: /account/infos?StatusUp=echec');
                exit();
            }
        }
    }

}