<?php


namespace controller;


class CommentController
{
    public function postComment(int $id_product){
        $content=htmlspecialchars($_POST['content']);
        if(isset($_SESSION['id'])){
            $id_account=$_SESSION['id'];
        }
        \model\CommentModel::insertComment($id_product,$id_account,$content);
            header("location:/store/$id_product");
            exit();
    }

}