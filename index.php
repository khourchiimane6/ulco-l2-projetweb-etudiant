<?php

/** Initialisation de l'autoloading et du router ******************************/

require('src/Autoloader.php');
Autoloader::register();

session_start();

$router = new router\Router(basename(__DIR__));

/** Définition des routes *****************************************************/

// GET "/"
$router->get('/', 'controller\IndexController@index');

/** définir une nouvelle route pour la boutique **/

$router->get('/store','controller\StoreController@store');

/** Route paramétrée store/id **/

$router->get('/store/{:num}','controller\StoreController@product');

/** route pour "/account" */
$router->get('/account','controller\AccountController@account');

/** routes en mode POST */


$router->post("/account/login", "controller\AccountController@login");

$router->post("/account/signin","controller\AccountController@signin");

$router->post('/postComment/{:num}','controller\CommentController@postComment');

$router->post('/store/search','controller\StoreController@search');

$router->post('/account/update','controller\AccountController@update');

$router->post('/cart/add','controller\CartController@add');


/** routes en mode GET */

$router->get('/account/logout','controller\AccountController@logout');

$router->get('/account/infos','controller\AccountController@infos');

$router->get('/cart','controller\CartController@cart');


// Erreur 404
$router->whenNotFound('controller\ErrorController@error');

/** Ecoute des requêtes entrantes *********************************************/

$router->listen();

